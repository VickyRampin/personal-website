.. title: Welcome!
.. slug: index
.. date: 2016-07-31 00:00:00 UTC
.. link:
.. description: Home page for Vicky Rampin (née Steeves).

.. image:: /images/selfie.jpg
   :align: left
   :class: face
   :scale: 60
   :alt: Vicky's face, a white woman with dark hair and blue eyes with black glasses

.. raw:: html

    <p class="lead">  Hi!! I'm Vicky Rampin (née Steeves). I am a librarian specializing in data management and reproducibility. I am an advocate for open scholarship, transparency, and justice.</p>

    <p>​I'm the Librarian for Research Data Management and Reproducibility, as well as the subject specialist for data science and computer science at New York University Divsion of Libraries. I support students, faculty, and staff in creating well-managed, high quality, and reproducible research.</p>
    
    <p>My research centers on integrating reproducible practices into the research workflow, advocating openness in all facets of research (manuscripts, code, data, analysis tools, etc.), and building/contributing to open infrastructure.​ I work on <a href="https://www.taguette.org/">Taguette</a>, an free and open source qualitative analysis tool, as well as <a href="https://www.reprozip.org/">ReproZip</a>, a free and open source computational reproducibility tool.</p>

    <p>I am an alum of the National Digital Stewardship Residency New York (2014-15 cohort), at the American Museum of Natural History, which sparked my love of data librarianship and underscore to me the importance of digital preservation to data librarianship. My project was to gain a broad overview of the extent and status of AMNH digital assets pertaining to Science. To do so I developed a structured interview guide designed to measure and describe scientific digital assets resulting in a metric to predict ongoing data curation needs.</p> 
    
    <p>Other interests include: gaming (pencil &amp; paper, Switch, and PC), crafting (embroidery, sewing, knitting, crocheting, quilting, scrapbooking--you name it!), animal care (especially my kitten Little Boss!), snowboarding, and gardening.</p>
