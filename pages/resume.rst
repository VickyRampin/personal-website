.. title: Resume
.. slug: resume
.. date: 2021
.. type: text

Download a PDF copy of my CV: https://gitlab.com/VickyRampin/cv/blob/main/original/vicky-cv.pdf

.. raw:: html

    <embed>
               
        <ul class="nav nav-tabs">
            <li class="nav-item"><a class="nav-link active show" data-toggle="tab" href="#spe">Speaker Bio</a></li>
            <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#int">Interests & Skills</a></li>
            <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#edu">Education</a></li>
            <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#emp">Employment</a></li>
            <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#pub">Publications</a></li>
            <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#pre">Presentations</a></li>
            <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#mis">Creative Works</a></li>
        </ul>

        <!- -----------------TAB CONTENT--------------------- --> 
        <div id="myTabContent" class="tab-content">
        
        <!- -----------------BEGIN SPEAKER BIO--------------------- -->
        <div class="tab-pane fade active show" id="spe">
            <h3 class="mt-3">Speaking Information</h4>
                <p>Vicky Rampin is the Librarian for Research Data Management and Reproducibility and the subject specialist for data science at New York University. Vicky supports researchers of all levels and disciplines in creating well-managed, reproducible scholarship through instruction, consultation, outreach, and infrastructure building. Her research centers on data and software preservation in service of long-term reproducibility, as well as how best to individualize reproducibility best practices. They loves all things open and contributes to a number of open projects as a part of her job at NYU and in her spare time.</p>
            </ul>
        </div>
        <!- -----------------END--------------------- -->
        

        <!- -----------------BEGIN INTERESTS--------------------- -->
        <div class="tab-pane fade" id="int">
            <h3 class="mt-3">Interests</h4>
            <p>Data management, reproducibility, labour theory, digital preservation, digital archiving, database management, web development, animal care, music, and creative writing.</p>
            <h3>Skills</h3>		 
            <ul>
            <li>Platforms: Microsoft Windows and Ubuntu Linux</li>
            <li>Programming Languages: Java, C, C++, Objective C, Python, R, Perl, PHP, SQL, HTML, CSS, JavaScript, XML, and Praat</li>
            <li>Library Standards: MARC, Dublin Core, DACS, EAD, and RDF</li>
            </ul>
        </div>
        <!- -----------------END--------------------- -->
        
        <!- -----------------BEGIN EDUCATION--------------------- -->
        <div class="tab-pane fade" id="edu">
            <h3 class="mt-3">New York University <small>New York, NY, USA</small></h3>
            <h4>Master of Computer Science <small>August 2022</small></h4>
                <p class="h5">Courant Institute of Mathematical Sciences</p>
                <p class="h5">GPA: 3.53</p>

            <h3 class="mt-3">Simmons College <small>Boston, MA, USA</small></h3>
            
            <h4>Master of Library and Information Science <small>August 2014</small></h4>
                <p class="h5">GPA: 3.85</p>

            <p class="h5">Research Opportunities</p>
                <ul><li><a href="http://gslis.simmons.edu/smallworld/smallworldProjectStaff.html">Small World Project.</a> Research done accompanying Dr. Kathy Wisser, March-June 2014. I provided software analysis using Gephi, a data visualization software, on researchers' social network analysis of historical relationships between literary figures.</li></ul>

            <h4>Bachelor of Science in Computer Science and Information Technology <small>May 2013</small></h4>
            <p class="h5">GPA: 3.75</p>
            <p class="h5">Honours Thesis: Computational Linguistic Approach to Inflection in Human Speech and Difference in Meaning</p>

            <p class="h5">Study Abroad: Celtic Studies, University College Cork, Cork, Ireland, Summer 2012</p>

            <p class="h5">Research Opportunities</p>
            <ul>
                <li>"A Computational Linguistics Approach to Inflection and Difference in Meaning". Research completed accompanying Dr. Nanette Veilleux, August 2012-August 2013.</li>
                <li>"No Place To Go: A Discussion on LGBTQ Youth Homelessness in Boston" Research completed as a part of the Simmons World Challenge, January 2011</li>
            </ul>
            
            <p class="h5">Activities:</p>
                <ul>
                    <li>Honours Program: Participant 2010-2013</li>
                    <li>CS-Math Liaison: President 2012-2013; Treasurer 2011-2012</li>
                    <li>Simmons College LGBTQA Alliance: President 2012-2013; Treasurer 2011-2012; Political Activism Chair 2010-2011</li>
                    <li>Simmons College SciFi and Fantasy Club: Founder and Participant 2010-2013</li>
                    <li>Simmons World Challenge: Participant January 2012</li>
                </ul>
                
            <p class="h5">Honours and Achievements:</p>
                <ul>
                    <li>College of Arts and Sciences Dean's Fellow Award; 2013-2014</li>
                    <li>The Computer Science Award, given for academic excellence in comp sci; 2013</li>
                    <li>Collaborative Research Experience for Undergraduates, $15,000 research grant, given to encourage women in the STEM fields to complete research studies in their field, 2012-2013</li>
                    <li>Alumnae Endowed Scholarship; 2012-2013</li>
                    <li>Simmons College Grant, given to aid in research costs for field research; 2012-2013</li>
                    <li>Bowker Award for Travel, given to undergraduates who demonstrate academic excellence while completing studies abroad; Summer 2012</li>
                    <li>Independant Order of Oddfellows Scholarship, 2010-2013</li>
                    <li>President Merit Scholarship, given to students whose academic achievement and personal qualities indicate they will perform to the highest academic level, 2010-2013</li>
                    <li>Simmons College 3-1 Program, where students complete an undergraduate degree in three years and a master's degree in one year, first participant; 2010-present</li>
                    <li>Simmons College Dean's List, 2010-2013</li>
                    <li>United Pilgrimage for Youth, sponsored participant; Summer 2009</li>	
                </ul>
        </div>
        <!- -----------------END EDUCATION--------------------- -->

        <!- -----------------BEGIN EMPLOYMENT--------------------- -->
        <div class="tab-pane fade" id="emp">
            <h3 class="mt-3">Visiting Professor <small>Pratt Institute</small></h3>
            <ul>
                <li>Fall 2018 - present </li>
                <li>Teach <a href="https://vickysteeves.gitlab.io/lis-628-datalibrarianship/">LIS 628: Data Librarianship & Management</a> for the School of Information Science</li>
            </ul>
            
            <h3>Librarian for Research Data Management and Reproducibility <br/><small>New York University</small></h3>
            <ul>
                <li>August 2015-present</li>
                <li>Dual appointment between NYU Division of Libraries and NYU Center for Data Science</li>
                <li>Provide instructional and consultation services in RDM and reproducibility to faculty and advanced students</li>
                <li>Advise researchers on how to meet the data management and open data requirements of publishers and federal funding agencies</li>
                <li>Moore/Sloan Data Science Environment team member; Reproducibility Working Group member and Libraries Working Group member</li>
                <li>Assist in efforts to design a data repository and storage infrastructure for researchers at the University.</li>
            </ul>
            
            <h3>Adjunct Professor <small>Simmons College</small></h3>
            <ul>
                <li>2017 - 2018</li>
                <li>Taught database management online for the School of Information and Library Science. Fall 2017 & Spring 2018</li>
            </ul>
            
            <h3>Interim Program Coordinator <small>Metropolitan New York Library Council</small></h3>
            <ul>
                <li>June 2015-July 2015; December 2015-May 2016</li>
                    <li>Day-to-day coordinator of METRO’s National Digital Stewardship Residency in New York <a href="http://ndsr.nycdigital.org/">(NDSR-NY)</a> program</li>
                <li>Contribute to project planning, communications, documentation, evaluations, outreach, and help maintain the program’s web presence and online platforms.</li>
                <li>Plan, organize, and help run NDSR-affiliated events, meetings, and workshops.</li>
                <li>Serve as a representative and contact for NDSR-NY program in collaboration with host institutions, NDSR residents and Library of Congress and NDSR-Boston program staff.</li>
            </ul>
            <h3>National Digital Stewardship Resident <small>American Museum of Natural History</small></h3>
            <ul>
                <li>September 2014-May 2015</li>
                <li>See my NDSR application video <a href="https://youtu.be/3oS4boUD9ms">here!</a></li>
                <li>Survey the Science divisions to better understand their data storage, curation, and preservation needs.</li>
                <li>Identify existing practices and policies for integrated data storage, access, and management.</li>
                <li>Recommend strategies to digitally preserve the scientific research at the AMNH.</li>
            </ul>
            <h3>Archives Intern <small>Sasaki Associates</small></h3>
            <ul>
                <li>January 2014-April 2014</li>
                <li>Process historical architectural material and write the accompanying finding aid.</li>
                <li>Create records for each collection processed and catalogue them in Koha ILS.</li>
                </ul>
            <h3>Dean's Fellow for Technology <small>Simmons College</small></h3>
            <ul>
                <li>September 2013-June 2014</li>
                <li>Manage social media technology for undergraduate science departments.</li>
                <li>Generate interest in STEM at Simmons through social media outreach to alumnae, current students, and prospective students, through working on content creation with faculty.</li>
            </ul>
            <h3>Technical Resource Assistant <small>Simmons College</small></h3>
            <ul>
                <li>September 2013-June 2014</li>
                <li>Provide technical instruction to students, staff, and faculty at the Graduate School of Library and Information Science (GSLIS).</li>
                <li>Troubleshoot hard/software issues within GSLIS for students, staff, and faculty.</li>
            </ul>
            <h3>Contracted Web Developer <small>IES Technical Sales</small></h3>
            <ul>
                <li>May 2013-September 2013</li>
                <li>Update the company website, developing and implementing design and operational upgrades.</li>
                <li>Assess and adapt to changing client needs through the development and deployment phase.</li>
            </ul>		
            <h3>Tutor in Computer Science <small>Simmons College</small></h3>
            <ul>
                <li>September 2012-2013</li>
                <li>Provide one-on-one tutoring for students in computer science classes.</li>
                <li>Lead group study sessions for upcoming evaluations, tests, and projects.</li>
            </ul>
            <h3>Campus Representative <small>Tutors For All</small></h3>
            <ul>
                <li>January 2012-May 2012</li>
                <li>Enhance Tutors for All's social media pages through graphic design work and constant updating.</li>
                <li>Generate new leads among college campus populations through social media outreach.</li>
                </ul>
            <h3>Teacher's Assistant in Computer Science <small>Simmons College</small></h3>
            <ul>
                <li>2011-2013</li>
                <li>Create classwork, homework, and quizzes for students to complete.</li>
                <li>Grade students' work and report these grades to the instructor.</li>
                </ul> 
            <h3>Lab Monitor for Computer Science Laboratory <small>Simmons College</small></h3>
            <ul>
                <li>2011-2013</li>
                <li>Assist students with troubleshooting soft/hardware issues.</li>
                <li>Repair and maintain computers in the computer science laboratory.</li>
            </ul>
            <h3>Server <small>Not Your Average Joe's</small></h3>
            <ul>
                <li>May 2011-August 2014</li>
                <li>Communicate positively and effectively with guests and coworkers.</li>
                <li>Assimilate guest information rapidly while anticipating guests' needs.</li>
            </ul>
            <h3>Lead Tutor <small>Tutors For All</small></h3>
            <ul>
                <li>September 2010-May 2011</li>
                <li>Mentor and lead a group of tutors, including reviewing and editing their lesson plans and progress reports.</li>
                <li>Evaluate student performance with progress reports and communicate that progress to their guardians.</li>
            </ul>	
            <h3>Choreographer assistant <small>Cape Ann Community Theatre</small></h3>
            <ul>
                <li>March 2010-June 2010</li>
                <li>Assist in choreographing dance routines for the musical "How to Succeed in Business Without Really Trying."</li>
                <li>Teach these routines to performers and provide dance direction throughout rehearsal process.</li>
            </ul>
            <h3>Page/Librarian's Assistant <small>Hamilton-Wenham Regional Public Library</small></h3>
            <ul>
                <li>September 2009- August 2010</li>
                <li>Re-shelve library materials after patrons have returned them.</li>
                <li>Remain flexible and assist librarians in their various projects around the library.</li>
            </ul>
            <h3>Instructor & Camp Counselor <small>Safe Harbor Tang Soo Do</small></h3>
            <ul>
                <li>May 2005-September 2009</li>
                <li>Teach a one-hour martial arts class for students ages 4-13, of all ranks, every morning.</li>
                <li>Lead the students in activities daily including arts and crafts, athletic activities, and many others.</li> 
            </ul>
        </div>
        <!- -----------------END EMPLOYMENT--------------------- -->

        <!- -----------------BEGIN PUBLICATIONS--------------------- -->
        <div class="tab-pane fade" id="pub">
        <div class="mt-3">
    </embed>
    
.. publication_list:: bibtex/publications.bib
    :style: alpha
    :detail_page_dir: 
    
.. raw:: html

    <embed>
        </div>
        </div>
        <!- -----------------END PUBLICATIONS--------------------- -->

        <!- -----------------BEGIN PRESENTATIONS--------------------- -->
        <div class="tab-pane fade" id="pre">
        <div class="mt-3">
    
    </embed>
     
.. publication_list:: bibtex/presentations.bib
    :style: alpha
    :detail_page_dir: 
    
.. raw:: html

    <embed>
        </div>
        </div>
        <!- -----------------END PRESENTATIONS--------------------- -->

        <!- -----------BEGIN OTHER OUTPUT----------------- -->
        <div class="tab-pane fade" id="mis">
        <div class="mt-3">
    
    </embed>
    
.. publication_list:: bibtex/codeMediaOthers.bib
    :style: alpha
    :detail_page_dir: 
    
.. raw:: html

    <embed>
        </div>
        </div>
        <!- -----------END OTHER OUTPUT----------------- -->
        
        </div>
        <!- -----------END TAB CONTENT----------------- -->

    </embed>
