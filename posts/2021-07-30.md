.. title: Environmental scan of decentralization and federation use in the academy
.. slug: ideas-03
.. date: 2021-07-30 21:48:00 EDT
.. link: https://gitlab.com/VickyRampin/personal-website/blob/main/posts/2021-07-30.md
.. category: ideas
.. status: draft
.. description: A blog post detailing some research I would do if I had time; this edition examines how to assess University-wide initiatives.
.. type: text

environmental scan of decentralization and federation use in the academy
