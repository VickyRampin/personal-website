.. title: Self-ethnography, looking at publicly available materials only
.. slug: ideas-04
.. date: 2021-08-06 21:48:00 EDT
.. link: https://gitlab.com/VickyRampin/personal-website/blob/main/posts/2021-08-06.md
.. category: ideas
.. status: draft
.. description: A blog post detailing some research I would do if I had time; this edition examines how to assess University-wide initiatives.
.. type: text

self-ethnography, looking at publicly available materials only
